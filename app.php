<?php

require __DIR__ . '/vendor/autoload.php';


use Silex\Application;
use Silex\Provider;
use Silex\Route;
use Symfony\Component\Yaml\Yaml;
use DerAlex\Silex\YamlConfigServiceProvider;

use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;


$app = new Application();

$app->register(new YamlConfigServiceProvider(__DIR__ . '/config/parameters.yml'));
$app->register(new YamlConfigServiceProvider(__DIR__ . '/config/connectors.yml'));


if (isset($app['config']['parameters']['system']['env'])
    && $app['config']['parameters']['system']['env'] == 'dev'
) {
    ErrorHandler::register();
    ExceptionHandler::register();
    $app->error(function (\Exception $e, $code) {
        error_log($e->getMessage());
    });
}

$app['cache_dir'] = __DIR__ . '/cache';

$app->register(new YamlConfigServiceProvider(__DIR__ . '/config/routing.yml'));
foreach ($app['config']['routing'] as $route) {

    $controller = $app
        ->match($route['pattern'], $route['controller']);

    if (isset($route['method'])) {
        $controller->method(isset($route['method']) ? $route['method'] : 'GET');
    }
}

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/src/Templates',
));


$app['mailer'] = $app->share(function () use ($app) {

    $options = $app['swiftmailer.options'] = $app['config']['connectors']['mailer'];

    switch ($options['transport']) {

        case 'phpmailer':

            require __DIR__ . '/lib/class.phpmailer.php';

            $mailer = new PHPMailer();
            $mailer->CharSet = 'UTF-8';

            return $mailer;

            break;

        case 'smtp':

            $transport = Swift_SmtpTransport::newInstance($options['host'], $options['port'])
                ->setUsername($options['username'])
                ->setPassword($options['password']);
            return Swift_Mailer::newInstance($transport);

            break;

        case 'sendmail':

            $transport = Swift_SendmailTransport::newInstance();
            return Swift_Mailer::newInstance($transport);

            break;

        default:
            throw new Exception(sprintf('Invalid swiftmailer transport %s', $options['transport']));
    }
});


$app['amo.delegate'] = $app->share(function () {
    return new \Amo\Service\Amo\Delegate();
});

$app['amo.connector'] = $app->share(function () use ($app) {
    require __DIR__ . '/lib/AmoRestApi.php';
    $settings = $app['config']['connectors']['amo'];
    $endpoint = new AmoRestApi(
        $settings['domain'],
        $settings['login'],
        $settings['apikey']
    );
    return new \Amo\Service\Amo\Connector($endpoint);
});

$app['amo.utility'] = $app->share(function () use ($app) {

    require __DIR__ . '/lib/smsimple.class.php';
    require __DIR__ . '/lib/unisender_api.php';

    $settings = $app['config']['connectors']['smsimple'];
    $smsimple = new SMSimple(array(
        'url' => 'http://api.smsimple.ru/',
        'username' => $settings['username'],
        'password' => $settings['password'],
    ));

    $settings = $app['config']['connectors']['unisender'];
    $unisender = new UniSenderApi($settings['apikey']);

    return new \Amo\Service\Amo\Utility($app, $smsimple, $unisender);
});


$app['invoker'] = $app->share(function () use ($app) {
    $app->register(new YamlConfigServiceProvider(__DIR__ . '/config/callbacks.yml'));
    $service = new \Amo\Service\Invoker();
    $service->register($app['config']['callbacks'], function ($service) use ($app) {
        return $app[$service];
    });
    return $service;
});

$config = $app['config'];
$config['config'] = Yaml::parse(file_get_contents(__DIR__ . '/config/config.yml'));
$app['config'] = $config;

$app['command'] = $app->share(function () use ($app) {
    $commands = \Symfony\Component\Yaml\Yaml::parse(file_get_contents(__DIR__ . '/config/commands.yml'));
    $service = new \Amo\Service\CommandService();
    $service->register($commands, function ($service) use ($app) {
        return $app[$service];
    });
    return $service;
});


/**
 * Run application
 */
$app['debug'] = $app['config']['parameters']['system']['debug'];
$app->run();