<?php

namespace Amo\Controller;

use Amo\Service\Invoker;
use Amo\Service\Invoker\Invokable;
use Amo\Service\Context;
use Amo\Service\Invoker\JsonController;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;


/**
 * Main delegate controller for handling Amo hooked callbacks.
 *
 * @class   Callback
 * @package Amo\Controller
 * @author  PM:/ <pm@spiral.ninja>
 */
class CallbackController implements Invokable
{
    public function __invoke(Request $request, Application $app, $method)
    {
        /** @var Invoker $invoker */
        $invoker = $app['invoker'];

        return $invoker->invoke($this, $method, Context::create(array(
            'request' => $request,
            'app'     => $app
        )));
    }

    public function __execute(Request $request, Application $app, $service, $method)
    {
        /** @var Invoker $invoker */
        $invoker = $app['invoker'];

        $context = Context::create(array(
            'request' => array(
                'request' => $request->request->all(),
                'query'   => $request->query->all()
            ),
            'app'     => $app
        ));

        $result = $invoker->execute($service, $method, $context);

        return $this->__processResult($result, $context);
    }

    /**
     * Method to be called if subject method was not found.
     *
     * @param string $method
     * @return mixed
     */
    public function __methodNotFound($method)
    {
        $response = new JsonResponse();

        return $response
            ->setEncodingOptions(
                $response->getEncodingOptions() | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
            )
            ->setData(array(
                'status' => 'error',
                'response' => sprintf('Method not found', $method)
            ))
            ->setStatusCode(404);
    }

    /**
     * Method to be called after successful invocation to modify result for output.
     *
     * @param mixed $result
     * @param Context $context
     * @return JsonResponse
     */
    public function __processResult($result, Context $context = null)
    {
        $response = new JsonResponse();

        return $response
            ->setEncodingOptions(
                $response->getEncodingOptions() | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
            )
            ->setData(array(
                'status' => 'success',
                'response' => $result
            ))
            ->setStatusCode(200);
    }

    public function __handleError(\Exception $e, Context $context)
    {
        $response = new JsonResponse();

        return $response
            ->setEncodingOptions(
                $response->getEncodingOptions() | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
            )
            ->setData(array(
                'status' => 'error',
                'response' => $e->getMessage()
            ))
            ->setStatusCode(500);
    }
}
