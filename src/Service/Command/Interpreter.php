<?php

namespace Amo\Service\Command;


use Amo\Service\Command\Types\CallbackCommand;
use Amo\Service\Command\Types\FetchCommand;
use Amo\Service\Command\Types\MapCommand;
use Amo\Service\Command\Types\MessengerCommand;
use Amo\Service\Command\Types\PropertyCommand;
use Amo\Service\Command\Types\ValidateCommand;

class Interpreter
{
    private $lookup;


    public function __construct($lookup)
    {
        $this->lookup = $lookup;
    }

    public function interpret($definition)
    {
        switch ($definition['type']) {

            case 'macro':
                $command = new Macro($definition, $this);
                break;

            case 'map':
                $command = new MapCommand($definition);
                break;

            case 'callback':
                $command = new CallbackCommand($definition, $this->lookup);
                break;

            case 'property':
                $command = new PropertyCommand($definition, $this->lookup);
                break;

            case 'fetch':
                $command = new FetchCommand($definition, $this->lookup);
                break;

            case 'validate':
                $command = new ValidateCommand($definition);
                break;

            case 'messenger':
                $command = new MessengerCommand($definition, $this->lookup);
                break;

            default:
                throw new \Exception(sprintf('Invalid command type %s', $definition['type']));

        }

        return $command;
    }
}