<?php

namespace Amo\Service\Command;


/**
 * Utility class to collect and render logged events for specific task.
 * Implemented as singleton to simplify things, but can be easily converted back to normal for flexibility
 * or should multiple instances be needed.
 *
 * @class   EventLogger
 * @package Amo\Service\Command
 * @author  PM:/ <pm@spiral.ninja>
 */
class EventLogger
{
    const EVENT_MESSAGE_SENT = 1;

    const EVENT_MESSAGE_FAILED = 2;


    /**
     * This array will also be used to validate passed event types
     *
     * @type array
     */
    private $eventTemplates = array(
        self::EVENT_MESSAGE_SENT   => 'logger/event/messageSent',
        self::EVENT_MESSAGE_FAILED => 'logger/event/messageFailed',
    );

    private $resultTemplate = 'logger/result';

    /**
     * @type \Twig_Environment
     */
    private $twig;

    /**
     * These parameters will be used to render result template.
     *
     * @type array
     */
    private $parameters;

    /**
     * Collected events
     *
     * @type array
     */
    private $events = array();


    private function __construct(\Twig_Environment $twig, $parameters = array())
    {
        $this->twig = $twig;
        $this->parameters = $parameters;
    }

    /**
     * @param \Twig_Environment $twig
     * @param array $parameters
     * @return EventLogger
     */
    public static function instance(\Twig_Environment $twig = null, $parameters = array())
    {
        static $instance;

        if (!isset($instance)) {
            $instance = new EventLogger($twig, $parameters);
        }

        return $instance;
    }

    /**
     * @param \Twig_Environment $twig
     * @param array $parameters
     * @return EventLogger
     */
    public static function create(\Twig_Environment $twig, $parameters = array())
    {
        return self::instance($twig, $parameters);
    }

    public function addEvent($type, $parameters, $template = null)
    {
        if (!isset($this->eventTemplates[ $type ])) {
            /** Unknown event type */
            throw new \Exception(sprintf('Unknown event type %s', $type));
        }

        if (!isset($template)) {
            $template = $this->eventTemplates[ $type ];
        }

        $this->events[] = array(
            'type'       => $type,
            'parameters' => $parameters,
            'template'   => $template
        );
    }

    public function getResult($template = null)
    {
        if (!isset($template)) {
            $template = $this->resultTemplate;
        }

        $events = array();

        foreach ($this->events as $event) {
            $events[] = $this->twig->render($event['template'] . '.twig', $event['parameters']);
        }

        return $this->twig->render($template . '.twig', array(
            'parameters' => $this->parameters,
            'events'     => $events
        ));
    }
}