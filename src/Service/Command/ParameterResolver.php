<?php

namespace Amo\Service\Command;


class ParameterResolver
{
    public static function resolveAll($definition, $context)
    {
        if (!isset($definition['parameters'])) {
            return array();
        }

        $parameters = array();

        foreach ($definition['parameters'] as $name => $parameter) {
            $parameters[$name] = static::resolveParameter($parameter, $context);
        }

        return $parameters;
    }

    private static function resolveParameter($parameter, $context)
    {
        if (isset($parameter['lookup'])) {
            $base = InputResolver::resolve($parameter['lookup'], $context);
        } elseif (isset($parameter['source']) && isset($parameter['query'])) {
            $base = InputResolver::query($parameter, $parameter['source'], $context);
        } else {
            // Exceptional point, avoid error by setting null base value
            $base = null;
        }

        if (isset($parameter['type'])) {
            switch ($parameter['type']) {
                case 'date':
                    $typed = new \DateTime($base);
                    if (isset($parameter['modify'])) {
                        $typed->modify($parameter['modify']);
                    }
                    break;

                default:
                    // Unknown type, set typed to base value to avoid exception
                    $typed = $base;
            }
            return $typed;
        } else {
            return $base;
        }
    }
}