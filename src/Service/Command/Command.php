<?php

namespace Amo\Service\Command;


use Amo\Service\Context;

abstract class Command implements Executable
{
    protected $definition;

    protected $config;


    public function __construct($definition)
    {
        $this->definition = $definition;
    }

    public function configure($config)
    {
        $this->config = $config;
    }

    protected function prepare(Context $context)
    {
        if (isset($this->config)) {
            $context['config'] = $this->config;
        }
    }

    protected function processResult($result, Context $context)
    {
        if (isset($this->definition['store'])) {
            $context[$this->definition['store']] = $result;
        }

        if (isset($this->definition['map'])) {

            $map = InputResolver::resolve($this->definition['map']['match'], $context);
            $mapped = FieldMapper::map($map, $this->definition['map'], $context);
            $result = array_merge($result, $mapped);

            if (isset($this->definition['store'])) {
                $context[$this->definition['store']] = $result;
            }
        }
    }
}