<?php

namespace Amo\Service\Command;


class FieldMapper
{
    public static function map($map, $definition, $context)
    {
        $result = array();

        foreach ($map as $store => $lookup) {
            if (is_scalar($lookup)) {
                $name = $lookup;
                /** @type bool $required Not required by default */
                $required = false;
                $default = '';
            } elseif (isset($lookup['name'])) {
                $name = $lookup['name'];
                $required = isset($lookup['required']) ? $lookup['required'] : false;
                $default = isset($lookup['default']) ? $lookup['default'] : '';
            } else {
                /** Lookup definition "name" parameter is mandatory */
                throw new \Exception(sprintf('Missed mandatory "name" parameter for mapped field "%s"', $store));
            }
            $field = static::lookupField($name, $definition['field'], $context);
            if (is_null($field) && $required) {
                throw new \Exception(sprintf('Failed to find required field %s in container %s',
                    $name, $definition['field']['source']));
            } elseif (is_null($field)) {
                $value = $default;
            } else {
                $value = static::lookupValue($field, $definition['value'], $context);
            }
            if (is_null($value) && $required) {
                throw new \Exception(sprintf('Failed to find required field value %s in container %s',
                    $name, $definition['value']['source']));
            } elseif (is_null($value)) {
                $value = $default;
            }
            $result[ $store ] = static::normalizeValue($value, $definition['value']);
        }

        return $result;
    }

    private static function normalizeValue($value, $definition)
    {
        if (is_scalar($value)) {
            return $value;
        }


        if (!isset($definition['cardinality']) || $definition['cardinality'] != 'list') {
            $value = $value[0];
            if (isset($definition['offset'])) {
                $value = $value[ $definition['offset'] ];
            }

            return $value;
        }

        $values = array();

        foreach ($value as $entry) {
            if (isset($definition['offset'])) {
                $values[] = $entry[ $definition['offset'] ];
            }
        }

        return $values;
    }

    private static function lookupField($field, $definition, $context)
    {
        $source = InputResolver::resolve($definition['source'], $context);

        foreach ($source as $entry) {
            $lookup = $entry[ $definition['lookup'] ];
            if ($lookup == $field) {
                return $entry[ $definition['return'] ];
            }
        }

        return null;
    }

    private static function lookupValue($field, $definition, $context)
    {
        $source = InputResolver::resolve($definition['source'], $context);

        if (is_null($source)) {
            return;
        }

        foreach ($source as $entry) {
            $lookup = $entry[ $definition['lookup'] ];
            if ($lookup == $field) {
                return $entry[ $definition['return'] ];
            }
        }

        return null;
    }
}