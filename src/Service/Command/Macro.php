<?php

namespace Amo\Service\Command;


use Amo\Service\Context;

class Macro extends Command implements \ArrayAccess
{
    private $commands = array();


    public function __construct($definition, Interpreter $interpreter)
    {
        foreach ($definition['commands'] as $name => $command) {
            $this->commands[$name] = $interpreter->interpret($command);
        }
    }

    public function execute(Context $context)
    {
        /** @var Command $command */
        foreach ($this->commands as $command) {
            $command->execute($context);
            if (isset($context['execution']['flow']) && $context['execution']['flow'] == 'break') {
                $context['result'] = $context['execution']['message'];
                break;
            }
        }

        return isset($context['result']) ? $context['result'] : '';
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        return isset($this->commands);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        return $this->commands[$offset];
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->commands[$offset] = $value;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->commands[$offset]);
    }
}