<?php

namespace Amo\Service\Command\Validators;


use Amo\Service\Command\InputResolver;

class RuleSet
{
    private $definition;

    private $rules;

    public function __construct($definition, $rules)
    {
        $this->definition = $definition;
        $this->rules = $rules;
    }

    /**
     * For now, only scalar input values checks are implemented
     *
     * @param $context
     * @return bool
     */
    public function validate($context)
    {
        foreach ($this->rules as $rule) {
            $field = static::lookupField($rule['field'], $this->definition['field'], $context);
            $value = static::lookupValue($field, $this->definition['value'], $context);
            $test = isset($rule['test']) ? $rule['test'] : null;
            if (is_scalar($value)) {
                if (!static::validateScalar($value, $rule['op'], $test)) {
                    return false;
                }
                continue;
            }
            if (is_array($value)) {
                $values = $value;
                foreach ($values as $value) {
                    if (isset($this->definition['value']['offset'])) {
                        $value = $value[$this->definition['value']['offset']];
                    }
                    if (!static::validateScalar($value, $rule['op'], $test)) {
                        return false;
                    }
                    continue 2;
                }
            }
        }

        return true;
    }

    private static function validateScalar($input, $op, $test = null)
    {
        switch ($op) {
            case 'eq':
            case '==':
                return $input == $test;

            case 'neq':
            case '!=':
                return $input != $test;

            case 'gt':
            case '>':
                return $input > $test;

            case 'lt':
            case '<':
                return $input < $test;

            case 'gte':
            case '>=':
                return $input >= $test;

            case 'lte':
            case '<=':
                return $input <= $test;

            case 'empty':
                return empty($input);

            case '!empty':
                return !empty($input);
        }
    }

    private static function lookupField($field, $definition, $context)
    {
        $source = InputResolver::resolve($definition['source'], $context);

        foreach ($source as $entry) {
            $lookup = $entry[$definition['lookup']];
            if ($lookup == $field) {
                return $entry[$definition['return']];
            }
        }

        return null;
    }

    private static function lookupValue($field, $definition, $context)
    {
        $source = InputResolver::resolve($definition['source'], $context);

        foreach ($source as $entry) {
            $lookup = $entry[$definition['lookup']];
            if ($lookup == $field) {
                return $entry[$definition['return']];
            }
        }

        return null;
    }
}