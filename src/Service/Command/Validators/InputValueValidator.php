<?php

namespace Amo\Service\Command\Validators;


use Amo\Service\Command\InputResolver;

class InputValueValidator extends Validator
{
    public function __construct($definition)
    {
        parent::__construct($definition);
    }

    public function validate($context)
    {
        $input = InputResolver::resolve($this->definition['input'], $context);

        $accept = static::mapAcceptValues($this->definition['accept'], $context);

        switch ($this->definition['rule']) {

            case 'in_array':
                return in_array($input, $accept);

            default:
                throw new \Exception(sprintf('Invalid validation rule %s', $this->definition['rule']));
        }
    }

    private static function mapAcceptValues($definition, $context)
    {
        $source = InputResolver::resolve($definition['source'], $context);

        $values = InputResolver::resolve($definition['values'], $context);

        $accept = array();

        foreach ($source as $entry) {
            $lookup = $entry[$definition['lookup']];
            if (in_array($lookup, $values)) {
                $accept[] = $entry[$definition['return']];
            }
        }

        return $accept;
    }
}