<?php

namespace Amo\Service\Command\Validators;

use Amo\Service\Command\InputResolver;


class ValuesChainValidator extends Validator
{
    public function __construct($definition)
    {
        parent::__construct($definition);
    }

    public function validate($context)
    {
        $input = InputResolver::resolve($this->definition['input'], $context);

        $ruleSet = new RuleSet(
            $this->definition,
            InputResolver::resolve($this->definition['ruleset'], $context)
        );

        return $ruleSet->validate($context);
    }
}