<?php

namespace Amo\Service\Command\Validators;


abstract class Validator
{
    protected $definition;


    public function __construct($definition)
    {
        $this->definition = $definition;
    }

    abstract public function validate($context);
}