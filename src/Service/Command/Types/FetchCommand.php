<?php

namespace Amo\Service\Command\Types;

use Amo\Service\Command\Command;
use Amo\Service\Command\InputResolver;
use Amo\Service\Context;
use Symfony\Component\HttpFoundation\Request;


class FetchCommand extends Command
{
    private $service;

    private $method;


    public function __construct($definition, $lookup)
    {
        parent::__construct($definition);

        $this->service = $lookup($definition['service']);
        $this->method = $definition['method'];

        if (!is_callable(array($this->service, $this->method))) {
            throw new \Exception('Service method %s::%s is not callable',
                $definition['service'], $this->method);
        }
    }

    public function execute(Context $context)
    {
        $object = $this->definition['object'];

        $parameters = $this->mapParameters($this->definition, $context);

        $results = $this->service->{$this->method}($object, $parameters);

        if (is_null($results)) {
            throw new \Exception(sprintf('Object %s (%s) not found',
                $this->definition['object'], join(', ', $parameters)));
        }

        $results = static::mapResults($this->definition, $results);

        $this->processResult($results, $context);

        return $results;
    }

    private static function mapParameters($definition, $context)
    {
        $source = array();

        foreach ($definition['source'] as $entry) {

            switch ($entry['type']) {

                case 'input':
                    /** @var Request $request */
                    $request = $context['request'];
                    $value = $request->get($entry['field']);
                    break;

                case 'lookup':
                    $value = InputResolver::resolve($entry['lookup'], $context);
                    break;

                default:
                    throw new \Exception(sprintf('Invalid source entry type %s', $entry['type']));
            }

            static::validateValue($value, $entry);
            $source[ $entry['map'] ] = $value;
        }

        $parameters = array();

        foreach ($definition['parameters'] as $map => $entry) {
            $parameters[ $map ] = $source[ $entry ];
        }

        return $parameters;
    }

    private static function mapResults($definition, $results)
    {
        switch ($definition['cardinality']) {

            case 'item':
                if (count($results) > 1) {
                    throw new \Exception('Multiple item results can not be converted to single cardinality item');
                }

                return $results[0];

            default:
                throw new \Exception(sprintf('Invalid result cardinality %s', $definition['cardinality']));
        }
    }

    private static function validateValue($value, $entry)
    {
        $field = isset($entry['field']) ? $entry['field'] : $entry['lookup'];

        if (is_null($value) && $entry['required']) {
            throw new \Exception(sprintf('Required parameter %s is not set in %s',
                $field, $entry['type']));
        }
        if (empty($value) && $entry['positive']) {
            throw new \Exception(sprintf('Required %s parameter %s is empty',
                $field, $entry['field']));
        }
    }
}