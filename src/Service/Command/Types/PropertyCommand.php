<?php

namespace Amo\Service\Command\Types;


use Amo\Service\Command\Command;
use Amo\Service\Context;


class PropertyCommand extends Command
{
    private $service;

    private $name;


    public function __construct($definition, $lookup)
    {
        parent::__construct($definition);

        $this->service = $lookup($definition['service']);
        $this->name = $definition['name'];
    }

    public function execute(Context $context)
    {
        $result = $this->service->{$this->name};

        $this->processResult($result, $context);

        return $result;
    }
}
