<?php

namespace Amo\Service\Command\Types;

use Amo\Service\Command\Command;
use Amo\Service\Command\InputResolver;
use Amo\Service\Context;


class CallbackCommand extends Command
{
    private $service;

    private $method;


    public function __construct($definition, $lookup)
    {
        parent::__construct($definition);

        $this->service = $lookup($definition['service']);
        $this->method = $definition['method'];

        if (!is_callable(array($this->service, $this->method))) {
            throw new \Exception('Service method %s::%s is not callable',
                $definition['service'], $this->method);
        }
    }

    public function execute(Context $context)
    {
        $parameters = array();

        if (isset($this->definition['parameters'])) {
            foreach ($this->definition['parameters'] as $name => $source) {
                $value = InputResolver::resolve($source, $context);
                $parameters[ $name ] = $value;
            }
        }

        $result = $this->service->{$this->method}($context, $parameters);

        $this->processResult($result, $context);

        return $result;
    }
}
