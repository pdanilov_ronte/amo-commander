<?php

namespace Amo\Service\Command\Types;

use Amo\Service\Command\Command;
use Amo\Service\Command\InputResolver;
use Amo\Service\Context;


class MapCommand extends Command
{
    public function __construct($definition)
    {
        parent::__construct($definition);
    }

    public function execute(Context $context)
    {
        $input = InputResolver::resolve($this->definition['input'], $context);

        if (!is_null($input)) {
            $this->set($this->definition['output'], $context, $input);
        }

        return $input;
    }

    protected function set($path, &$container, $value)
    {
        $stop = strpos($path, '.');

        if ($stop === false) {
            $container[ $path ] = $value;
        } else {
            $offset = substr($path, 0, $stop);
            if (!isset($container[ $offset ])) {
                $container[ $offset ] = array();
            }
            /**
             * Workaround for overloaded member indirect modification
             */
            $box = $container[ $offset ];
            $this->set(substr($path, $stop + 1), $box, $value);
            $container[ $offset ] = $box;
        }
    }
}