<?php

namespace Amo\Service\Command\Types;


use Amo\Service\Command\Command;
use Amo\Service\Command\Validators\InputValueValidator;
use Amo\Service\Command\Validators\Validator;
use Amo\Service\Command\Validators\ValuesChainValidator;
use Amo\Service\Context;


class ValidateCommand extends Command
{
    /**
     * @var Validator
     */
    private $validator;


    public function __construct($definition)
    {
        parent::__construct($definition);

        $this->validator = static::createValidator($definition);
    }

    public function execute(Context $context)
    {
        $result = $this->validator->validate($context);

        if ($result == false) {
            $context['execution'] = array(
                'flow' => 'break',
                'message' => isset($context['message']) ? $context['message'] : 'Validation failed'
            );
        }
    }

    /**
     * @param $definition
     * @return Validator
     * @throws \Exception
     */
    public static function createValidator($definition)
    {
        switch ($definition['validator']) {

            case 'input.value':
                return new InputValueValidator($definition);

            case 'input.values_chain':
                return new ValuesChainValidator($definition);

            default:
                throw new \Exception(sprintf('Validator %s not found', $definition['validator']));
        }
    }
}