<?php

namespace Amo\Service\Command\Types;

use Amo\Service\Amo\Utility;
use Amo\Service\Command\Command;
use Amo\Service\Command\EventLogger;
use Amo\Service\Command\InputResolver;
use Amo\Service\Context;


class MessengerCommand extends Command
{
    /**
     * @var Utility
     */
    private $service;

    /**
     * @type EventLogger
     */
    private $logger;


    public function __construct($definition, $lookup)
    {
        parent::__construct($definition);

        $this->service = $lookup('amo.utility');

        $this->logger = EventLogger::create($lookup('twig'), $definition);
    }

    public function execute(Context $context)
    {
        $result = '';

        $recipients = InputResolver::resolve($this->definition['recipients'], $context);

        foreach ($recipients as $recipient) {

            $list = static::resolveRecipients($recipient, $this->definition, $context);

            $this->service->sendNotification($list, $recipient, $context);
        }

        $this->processResult($result, $context);

        $this->service->logEvents($this->logger->getResult(), $context);

        return $result;
    }

    private static function resolveRecipients($recipient, $definition, $context)
    {
        if (isset($recipient['list'])) {
            $lists = InputResolver::resolve($definition['lists'], $context);
            return InputResolver::resolve($recipient['list'], $lists);
        }

        if (isset($recipient['book'])) {
            $book = InputResolver::resolve($definition['book'], $context);
            $address = static::searchAddressBook($recipient['book'], $book, $context);
            return array($address);
        }

        $address = array();

        if (isset($recipient['name'])) {
            $address['name'] = static::resolveRecipientName($recipient['name'], $context);
        }

        foreach (array('user', 'lead', 'phone', 'email', 'from', 'subject') as $field) {
            if (isset($recipient[$field])) {
                $address[$field] = static::resolveRecipientAddress($recipient[$field], $context);
            }
        }

        return array($address);
    }

    private static function resolveRecipientAddress($address, $context)
    {
        if (is_scalar($address)) {
            return $address;
        }

        if (isset($address['lookup'])) {
            return InputResolver::resolve($address['lookup'], $context);
        }

        return null;
    }

    private static function searchAddressBook($query, $book, $context)
    {
        if (!isset($query['type'])) {
            throw new \Exception('Type must be defined in address book query');
        }

        if (!isset($book[$query['type']])) {
            throw new \Exception(sprintf('Address book type %s in not defined', $query['type']));
        }

        if (!isset($query['name'])) {
            throw new \Exception('Name must be defined in address book query');
        }

        if (is_array($query['name'])) {
            if (!isset($query['name']['lookup'])) {
                throw new \Exception('Lookup parameters is missing in address book name query definition');
            }

            if (empty($query['name']['lookup'])) {
                throw new \Exception('Lookup parameters is empty in address book name query definition');
            }

            $name = InputResolver::resolve($query['name']['lookup'], $context);

            if (empty($name)) {
                throw new \Exception(sprintf('Name lookup %s for address book query returned empty results',
                    $query['name']['lookup']));
            }

        } elseif (empty($query['name'])) {

            throw new \Exception('Name parameter for address book query cannot be empty');

        } else {

            $name = $query['name'];
        }

        foreach ($book[$query['type']] as $entry) {
            if ($entry['name'] == $name) {
                return $entry;
            }
        }

        return null;
    }

    private static function resolveRecipientName($name, $context)
    {
        if (is_scalar($name)) {
            return $name;
        }

        return InputResolver::resolve($name['lookup'], $context);
    }
}
