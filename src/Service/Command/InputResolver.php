<?php

namespace Amo\Service\Command;


class InputResolver
{
    public static function resolve($query, $context)
    {
        return static::_resolve(explode('.', $query), $context);
    }

    public static function query($query, $source, $context)
    {
        if (is_scalar($source)) {
            $data = static::resolve($source, $context);
            if (is_null($data)) {
                throw new \Exception(sprintf('Failed to resolve %s source from context', $source));
            } else if (!is_array($data)) {
                throw new \Exception(sprintf('Resolved %s source must be an array to being able to be queried', $source));
            }
            $source = $data;
        }
    }

    private static function _resolve($stack, $data)
    {
        $bottom = array_shift($stack);

        if (!isset($data[$bottom])) {
            return null;
        }

        if (count($stack)) {
            return static::_resolve($stack, $data[$bottom]);
        }

        return $data[$bottom];
    }
}