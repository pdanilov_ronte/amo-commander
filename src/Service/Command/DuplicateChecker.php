<?php

namespace Amo\Service\Command;


/**
 * This class is a temporary measure to make a workaround for duplicate notification messages.
 * Will be deprecated when more flexible solution is found.
 *
 * @class   DuplicateChecker
 * @package Amo\Service\Command
 * @author  PM:/ <pm@spiral.ninja>
 */
class DuplicateChecker
{
    /**
     * Checks whether combination of lead, recipient and message
     * has already been sent before and thus is duplicate.
     *
     * @param $service
     * @param $entry
     * @param $message
     * @param $data
     * @param $context
     * @return bool
     * @throws \Exception
     */
    public static function isDuplicate($service, $entry, $message, $data, $context)
    {
        /**
         * NOTE: Here assumed that lead.id is set in context,
         * thus making the checker strictly situational.
         */
        $lead_id = InputResolver::resolve('lead.id', $context);

        $key = self::generateKey($service, $entry, $message, $data);

        $cache_dir = $context['app']['cache_dir'];
        $cache_file = $cache_dir . '/' . $lead_id;

        if (is_file($cache_file)) {
            $cache = unserialize(file_get_contents($cache_file));
            if (isset($cache[ $key ])) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $service
     * @param $entry
     * @param $message
     * @param $data
     * @param $context
     * @throws \Exception
     */
    public static function saveEntry($service, $entry, $message, $data, $context)
    {
        $lead_id = InputResolver::resolve('lead.id', $context);

        $key = self::generateKey($service, $entry, $message, $data);

        $cache_dir = $context['app']['cache_dir'];
        $cache_file = $cache_dir . '/' . $lead_id;

        if (is_file($cache_file)) {
            $cache = unserialize(file_get_contents($cache_file));
            if (!isset($cache[ $key ])) {
                $cache[ $key ] = 1;
                file_put_contents($cache_file, serialize($cache));
            }
        } else {
            $cache = array(
                $key => 1
            );
            file_put_contents($cache_file, serialize($cache));
        }
    }

    /**
     * Generate md5 key for message parameters
     *
     * @param $service
     * @param $entry
     * @param $message
     * @param $data
     * @return string
     * @throws \Exception
     */
    public static function generateKey($service, $entry, $message, $data)
    {
        switch ($service) {
            case 'smsimple':
            case 'unisender':
                $key = md5(sprintf('%s-%s-%s', $service, $entry['phone'], $message));
                break;
            case 'email':
                $key = md5(sprintf('%s-%s-%s', $service, $entry['email'], $message));
                break;
            case 'task':
                /** @type \DateTime $complete_till */
                $complete_till = $data['parameters']['complete_till'];
                $key = md5(sprintf('%s-%s-%s-%s', $service, $entry['user'], $complete_till->format('Y-m-d h:i:s'), $message));
                break;
            default:
                throw new \Exception(
                    sprintf('Cannot perform duplicate check for service "%s" - service is unknown', $service));
        }

        return $key;
    }
}
