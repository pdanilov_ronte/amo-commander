<?php

namespace Amo\Service\Command;


use Amo\Service\Context;

interface Executable
{
    public function execute(Context $context);
}