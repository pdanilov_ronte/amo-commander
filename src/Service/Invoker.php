<?php

namespace Amo\Service;

use Amo\Service\Invoker\Callback as InvokerCallback;
use Amo\Service\Invoker\Invokable;


/**
 * @class Invoker
 * @package Amo\Service
 * @author PM:/ <pm@spiral.ninja>
 */
class Invoker
{
    private $callbacks = array();


    /**
     * @param array $definitions
     * @param callable $lookup
     */
    public function register(array $definitions, $lookup)
    {
        foreach ($definitions as $name => $definition) {
            $object = $definition['object'];
            if (!isset($this->callbacks[$object])) {
                $this->callbacks[$object] = array();
            }
            $method = $definition['method'];
            $service = $lookup($definition['service']);
            $this->callbacks[$object][$method] = new InvokerCallback($service, $definition['command']);
        }
    }

    public function execute($object, $method, Context $context)
    {
        if (!isset($this->callbacks[$object][$method])) {
            throw new \Exception(sprintf('Method %s::%s not found', $object, $method));
        }

        /** @type InvokerCallback $callback */
        $callback = $this->callbacks[$object][$method];

        $context['object'] = $object;
        $context['method'] = $method;


        return $callback->execute($context);
    }

    public function invoke(Invokable $delegate, $method, Context $context = null)
    {
        if (!preg_match('|^[\w\d]+$|', $method) || !method_exists($delegate, $method)) {
            return $delegate->__methodNotFound($method, $context);
        }

        try {

            return $delegate->__processResult($delegate->{$method}($context), $context);

        } catch (\Exception $e) {

            // @todo Send record to Monolog

            return $delegate->__handleError($e, $context);
        }
    }
}
