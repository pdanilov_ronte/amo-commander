<?php

namespace Amo\Service\Amo;


/**
 * @property array account
 */
class Connector
{
    private static $objects = array(
        'lead' => array(
            'list' => array('method' => 'getLeadsList', 'results' => 'leads')
        )
    );


    /**
     * @var \AmoRestApi
     */
    private $endpoint;

    private $properties = array();


    /**
     * @param \AmoRestApi $endpoint
     */
    public function __construct($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    public function preload()
    {
        $this->properties['account'] = $this->endpoint->getAccountInfo();
    }

    public function fetch($object, $parameters)
    {
        if (!isset(static::$objects[ $object ])) {
            throw new \Exception(sprintf('Object %s is not defined', $object));
        }

        $result = $this->endpoint->{static::$objects[ $object ]['list']['method']}(
            null,
            null,
            isset($parameters['ids']) ? $parameters['ids'] : null,
            isset($parameters['query']) ? $parameters['query'] : null
        );

        return $result[ static::$objects[ $object ]['list']['results'] ];
    }

    public function getLinkedContact($context, $parameters)
    {
        $result = $this->endpoint->getContactsLinks(null, null, null, $parameters['id']);

        if (empty($result['links'])) {
            return null;
        }

        $contact_id = $result['links'][0]['contact_id'];

        $contact = $this->endpoint->getContactsList(null, null, $contact_id);

        return $contact['contacts'][0];
    }

    /**
     * Creates task by given parameters
     *
     * @param $task
     * @return mixed
     * @throws \Exception
     */
    public function createTask($task)
    {
        $request = json_encode(array(
            'request' => array('tasks' => array('add' => array($task)))
        ));
        $response = $this->endpoint->setTasks($request);

        if (!isset($response['tasks']['add'][0])) {
            throw new \Exception('Add task error: malformed response from AMO service');
        }

        return $response['tasks']['add'][0];
    }

    public function createNote($note)
    {
        $this->endpoint->setNotes(
            array(
                'request' => array(
                    'notes' => array(
                        'add' => array($note)
                    )
                )
            )
        );
    }

    public function __get($name)
    {
        if (!isset($this->properties[ $name ])) {
            throw new \Exception(sprintf('Property %s is not defined', $name));
        }

        return $this->properties[ $name ];
    }
}