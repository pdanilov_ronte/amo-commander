<?php

namespace Amo\Service\Amo;


use Amo\Service\Command\DuplicateChecker;
use Amo\Service\Command\EventLogger;
use Amo\Service\Command\InputResolver;
use Amo\Service\Command\ParameterResolver;
use Exception;
use Silex\Application;

class Utility
{
    private $smsimple;

    private $unisender;

    /**
     * @var Connector
     */
    private $amo;

    /**
     * @type \PHPMailer
     */
    private $mailer;

    /**
     * @var \Twig_Environment
     */
    private $twig;


    public function __construct(Application $app, $smsimple, $unisender)
    {
        $test = $app['config']['parameters']['system']['test'];

        $this->smsimple = new SmSimpleConnector($smsimple,
            $app['config']['connectors']['smsimple']['origin_id'], $test);
        $this->unisender = new UnisenderConnector($unisender,
            $app['config']['connectors']['unisender']['sender'], $test);

        $this->amo = $app['amo.connector'];

        $this->mailer = $app['mailer'];

        $this->twig = $app['twig'];
    }

    public function sendNotification($list, $definition, $context)
    {
        $data = iterator_to_array($context);

        $data['parameters'] = ParameterResolver::resolveAll($definition, $context);

        foreach ($list as $entry) {

            /** Merge particular recipient entry with definition */
            $entry = array_merge($definition, $entry);

            $data['recipient'] = $entry;

            $message = $this->twig->render($definition['template'] . '.twig', $data);

            /** Check that message and recipient pair is unique for the lead */
            if (DuplicateChecker::isDuplicate(
                $definition['service'],
                $entry,
                $message,
                $data,
                $context)
            ) {
                /** This message is duplicate, simply do nothing and return */
                return;
            }

            switch ($definition['service']) {

                case 'smsimple':
                    try {
                        $result = $this->smsimple->send($entry['phone'], $message);
                        $success = (bool)$result;
                        $template = $success ? 'logger/event/smsSent' : 'logger/event/smsFailed';
                    } catch (\Exception $e) {
                        $result = $e->getMessage();
                        $success = false;
                        $template = 'logger/event/smsFailed';
                    }
                    break;

                case 'unisender':
                    try {
                        $result = $this->unisender->send($entry['phone'], $message);
                        $success = (bool)$result;
                        $template = $success ? 'logger/event/smsSent' : 'logger/event/smsFailed';
                    } catch (\Exception $e) {
                        $result = $e->getMessage();
                        $success = false;
                        $template = 'logger/event/smsFailed';
                    }
                    break;

                case 'email':
                    try {
                        $result = $this->sendEmail($entry, $message, $context);
                        $success = (bool)$result;
                        $template = $success ? 'logger/event/emailSent' : 'logger/event/emailFailed';
                    } catch (\Exception $e) {
                        $result = $e->getMessage();
                        $success = false;
                        $template = 'logger/event/emailFailed';
                    }
                    break;

                case 'task':
                    try {
                        $result = $this->createTask($entry, $data['parameters'], $message);
                        $success = (bool)$result;
                        if (!$success) {
                            $template = 'logger/event/taskFailed';
                        }
                    } catch (\Exception $e) {
                        $result = $e->getMessage();
                        $success = false;
                        $template = 'logger/event/taskFailed';
                    }
                    break;

                default:
                    throw new Exception(sprintf('Notification service %s is not defined', $definition['service']));
            }

            /** If all three parameters where defined then the note should be created */
            if (isset($result) && isset($success) && isset($template)) {

                $parameters = array_merge(
                    $entry,
                    array(
                        'result'  => $result,
                        'service' => $definition['service'],
                        'message' => $message
                    )
                );

                if ($success) {
                    $event = EventLogger::EVENT_MESSAGE_SENT;
                } else {
                    $event = EventLogger::EVENT_MESSAGE_FAILED;
                }

                EventLogger::instance()->addEvent($event, $parameters, $template);
            }

            /** In case of successful operation duplicate check entry should be added */
            if ($success) {
                DuplicateChecker::saveEntry($definition['service'], $entry, $message, $data, $context);
            }
        }
    }

    /**
     * Create lead note to store processing result.
     *
     * @param string $events
     * @param $context
     */
    public function logEvents($events, $context)
    {
        $note = array(
            'element_id'   => $context['lead']['id'],
            'element_type' => 2,
            'note_type'    => 4,
            'text'         => $events,
        );

        $this->amo->createNote($note);
    }

    private function sendEmail($recipient, $body, $context)
    {
        $from = isset($recipient['from'])
            ? $recipient['from']
            : $context['config']['mailer']['from'];

        $fromName = isset($recipient['fromName'])
            ? $recipient['fromName']
            : $context['config']['mailer']['fromName'];

        $subject = isset($recipient['subject'])
            ? $recipient['subject']
            : 'Notification';

        $this->mailer->From = $from;
        $this->mailer->FromName = $fromName;
        $this->mailer->addAddress($recipient['email']);

        $this->mailer->isHTML(true);
        $this->mailer->Subject = $subject;
        $this->mailer->Body = $body;

        $sent = $this->mailer->send();

        if(!$sent) {
            throw new \Exception(sprintf('Failed to send email: %s', $this->mailer->ErrorInfo));
        }

        return $sent;
    }

    /**
     * @param $recipient
     * @param $parameters
     * @param $message
     * @return mixed
     * @throws Exception
     */
    private function createTask($recipient, $parameters, $message)
    {
        if (isset($parameters['complete_till'])) {
            if (get_class($parameters['complete_till']) == 'DateTime') {
                /** @type \DateTime $date */
                $date = $parameters['complete_till'];
                $complete_till = $date->getTimestamp();
            } elseif (is_scalar($parameters['complete_till'])) {
                $complete_till = $parameters['complete_till'];
            } else {
                throw new \Exception(sprintf('Invalid type for "complete_till" parameter (%s)',
                    $parameters['complete_till']));
            }
        } else {
            $complete_till = time();
        }

        $task = array(
            'element_id'          => $recipient['lead'],
            'element_type'        => 2,
            'task_type'           => '',
            'text'                => $message,
            'responsible_user_id' => $recipient['user'],
            'complete_till'       => $complete_till
        );

        $task = $this->amo->createTask($task);

        if (!$task['id']) {
            throw new Exception('Failed to create task');
        }

        return $task['id'];
    }

    private static function resolveParameters($definition, $context)
    {
        if (!isset($definition['parameters'])) {
            return array();
        }

        $parameters = array();

        foreach ($definition['parameters'] as $name => $parameter) {
            $parameters[ $name ] = static::resolveParameter($parameter, $context);
        }

        return $parameters;
    }

    private static function resolveParameter($parameter, $context)
    {
        //{ source: config.address_book.worker, query: { name: { lookup: lead.worker } } }
        if (isset($parameter['lookup'])) {
            return InputResolver::resolve($parameter['lookup'], $context);
        } elseif (isset($parameter['source']) && isset($parameter['query'])) {
            //$source = InputResolver::resolve($parameter['source'], $context);
            //return static::querySource($parameter['query'], $source);
            return InputResolver::query($parameter, $parameter['source'], $context);
        }
    }

    private static function querySource($query, $source)
    {
        foreach ($query as $field => $condition) {
            if (is_scalar($condition)) {

            }
        }
    }
}


/**
 * @class SmSimpleConnector
 *
 * SMSimple API connector class
 */
class SmSimpleConnector
{
    /**
     * @var \SMSimple
     */
    private $service;

    private $origin_id;

    private $test = false;


    public function __construct($service, $origin_id, $test = false)
    {
        $this->service = $service;
        $this->origin_id = $origin_id;

        $this->test = $test;

        if ($this->test) {
            return;
        }

        try {
            $this->service->connect();
        } catch (Exception $e) {
            throw new Exception(sprintf('Failed to connect to SmSimple: %s', $e->getMessage()));
        }
    }

    public function send($phone, $message)
    {
        if ($this->test) {
            return null;
        }

        return $this->service->send($this->origin_id, (string)$phone, $message);
    }
}


/**
 * @class UnisenderConnector
 *
 * UniSender API connector class
 */
class UnisenderConnector
{
    /**
     * @var \UniSenderApi
     */
    private $service;

    private $sender;

    private $test = false;


    public function __construct($service, $sender, $test = false)
    {
        $this->service = $service;
        $this->sender = $sender;

        $this->test = $test;
    }

    /**
     * @param $phone
     * @param $message
     * @return bool
     * @throws Exception
     */
    public function send($phone, $message)
    {
        if ($this->test) {
            return null;
        }

        /** @noinspection PhpUndefinedMethodInspection */
        $result = $this->service->sendSms(array(
            "phone"  => $phone,
            "sender" => $this->sender,
            "text"   => $message
        ));

        if (isset($result['error'])) {
            throw new \Exception(sprintf('Failed to send unisender sms: %s', $result['error']));
        }

        return $result;
    }
}

