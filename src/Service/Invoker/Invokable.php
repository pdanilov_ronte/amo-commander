<?php

namespace Amo\Service\Invoker;
use Amo\Service\Context;


/**
 * @interface Invokable
 * @package Amo\Service\Invoker
 * @author PM:/ <pm@spiral.ninja>
 */
interface Invokable
{
    /**
     * Method to be called when subject method not found.
     *
     * @param string $method
     * @return mixed
     */
    public function __methodNotFound($method);

    /**
     * Method to be called after successful invocation to modify result for output.
     *
     * @param mixed $result
     * @param Context $context
     * @return mixed
     */
    public function __processResult($result, Context $context);

    public function __handleError(\Exception $e, Context $context);
}
