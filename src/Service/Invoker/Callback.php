<?php

namespace Amo\Service\Invoker;


use Amo\Service\Context;

class Callback
{
    private $service;

    private $command;


    public function __construct($service, $command)
    {
        $this->service = $service;
        $this->command = $command;
    }

    public function execute(Context $context)
    {
        if (!is_callable(array($this->service, $this->command))) {
            throw new \Exception(sprintf('Command %s::%s is not callable',
                get_class($this->service),
                $this->command
            ));
        }

        return $this->service->{$this->command}($context);
    }
}