<?php

namespace Amo\Service\Invoker;


abstract class Controller implements Invokable
{
    /**
     * Method to be called when subject method not found.
     *
     * @param string $method
     * @param Pod $pod
     * @return mixed
     */
    public function methodNotFound($method, Pod $pod = null)
    {
        // TODO: Implement methodNotFound() method.
    }

    /**
     * Method to be called after successful invocation to modify result for output.
     *
     * @param mixed $result
     * @param Pod $pod
     * @return mixed
     */
    public function processResult($result, Pod $pod = null)
    {
        // TODO: Implement processResult() method.
    }
}