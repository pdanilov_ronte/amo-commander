<?php

namespace Amo\Service;


use Amo\Service\Command\Command;
use Amo\Service\Command\Executable;
use Amo\Service\Command\Interpreter;


class CommandService
{
    private $commands = array();


    function register($definitions, $lookup)
    {
        $interpreter = new Interpreter($lookup);

        foreach ($definitions as $name => $definition) {
            $this->commands[$name] = $interpreter->interpret($definition);
        }
    }

    function execute(Context $context)
    {
        $method = $context['method'];

        /** @var Command $command */
        $command = $this->commands[$method];

        $config = $context['app']['config']['config'][$method];
        if ($config) {
            $context['config'] = $config;
        }

        $command->execute($context);

        return $context['result'];
    }
}