<?php

/**
 * Track the script total execution time
 *
 * @class  Profile
 * @author PM:/ <pm@spiral.ninja>
 */
class Profile
{
    const TRACE_OUTPUT = false;

    static private $start;

    static public function start()
    {
        self::$start = microtime(true);
    }

    static public function stop()
    {
        return number_format(microtime(true) - self::$start, 15);
    }
}

Profile::start();


/**
 * Get method name from request and call it asynchronously
 *
 * @throws Exception
 */
function receive()
{
    if (!isset($_GET['method']) || empty($_GET['method'])) {
        throw new \Exception('Requested method name has to be passed in request');
    }

    /**
     * In fact, it is not necessary to define methods here,
     *  as they already has to be defined in application callbacks list.
     * Yet checking that preliminary can save from some redundant calls to the main app.
     */
    switch ($_GET['method']) {
        case 'measure_notifier':
            $path = '/callback/lead/measure_notifier.json';
            break;
        case 'mount_notifier':
            $path = '/callback/lead/mount_notifier.json';
            break;
        default:
            throw new \Exception(sprintf('Method %s is not defined in application', $_GET['method']));
    }

    post($_SERVER['HTTP_HOST'], $path, http_build_query($_POST));
}

/**
 * Make asynchronous post request by opening a socket and closing it prematurely after sending the data.
 *
 * @param string $host
 * @param string $path
 * @param string $data
 * @throws Exception
 */
function post($host, $path, $data)
{
    $fp = fsockopen($host, 80, $errno, $errstr, 0.4);

    if (!$fp) {
        throw new \Exception(sprintf('Failed to open a socket to %s (%s: %s)', $host, $errno, $errstr));
    }

    $out = "POST " . $path . " HTTP/1.1\r\n";
    $out .= "Host: " . $host . "\r\n";
    $out .= "Content-Type: application/x-www-form-urlencoded\r\n";
    $out .= "Content-Length: " . strlen($data) . "\r\n";
    $out .= "Connection: Close\r\n\r\n";
    $out .= $data;

    fwrite($fp, $out);

    if (Profile::TRACE_OUTPUT) {
        while (!feof($fp)) {
            print fgets($fp, 128);
        }
    }

    fclose($fp);
}


header("HTTP/1.1 200 OK");

try {
    receive();
} catch (\Exception $e) {
    print json_encode(array(
        'status'         => 'failure',
        'result'         => $e->getMessage(),
        'execution_time' => Profile::stop()
    ));
    exit;
}

print json_encode(array(
    'status'         => 'finished',
    'result'         => 'success',
    'execution_time' => Profile::stop()
));

exit;
